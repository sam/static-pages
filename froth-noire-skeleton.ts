import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const frothNoire: CustomThemeConfig = {
	name: 'froth-noire',
	properties: {
		// =~= Theme Properties =~=
		'--theme-font-family-base': `system-ui`,
		'--theme-font-family-heading': `system-ui`,
		'--theme-font-color-base': '0 0 0',
		'--theme-font-color-dark': '255 255 255',
		'--theme-rounded-base': '4px',
		'--theme-rounded-container': '4px',
		'--theme-border-base': '1px',
		// =~= Theme On-X Colors =~=
		'--on-primary': '0 0 0',
		'--on-secondary': '0 0 0',
		'--on-tertiary': '0 0 0',
		'--on-success': '0 0 0',
		'--on-warning': '0 0 0',
		'--on-error': '0 0 0',
		'--on-surface': '255 255 255',
		// =~= Theme Colors  =~=
		// primary | #A0A0A0
		'--color-primary-50': '241 241 241', // #f1f1f1
		'--color-primary-100': '236 236 236', // #ececec
		'--color-primary-200': '231 231 231', // #e7e7e7
		'--color-primary-300': '217 217 217', // #d9d9d9
		'--color-primary-400': '189 189 189', // #bdbdbd
		'--color-primary-500': '160 160 160', // #A0A0A0
		'--color-primary-600': '144 144 144', // #909090
		'--color-primary-700': '120 120 120', // #787878
		'--color-primary-800': '96 96 96', // #606060
		'--color-primary-900': '78 78 78', // #4e4e4e
		// secondary | #bcbcbc
		'--color-secondary-50': '245 245 245', // #f5f5f5
		'--color-secondary-100': '242 242 242', // #f2f2f2
		'--color-secondary-200': '238 238 238', // #eeeeee
		'--color-secondary-300': '228 228 228', // #e4e4e4
		'--color-secondary-400': '208 208 208', // #d0d0d0
		'--color-secondary-500': '188 188 188', // #bcbcbc
		'--color-secondary-600': '169 169 169', // #a9a9a9
		'--color-secondary-700': '141 141 141', // #8d8d8d
		'--color-secondary-800': '113 113 113', // #717171
		'--color-secondary-900': '92 92 92', // #5c5c5c
		// tertiary | #c0c0c0
		'--color-tertiary-50': '246 246 246', // #f6f6f6
		'--color-tertiary-100': '242 242 242', // #f2f2f2
		'--color-tertiary-200': '239 239 239', // #efefef
		'--color-tertiary-300': '230 230 230', // #e6e6e6
		'--color-tertiary-400': '211 211 211', // #d3d3d3
		'--color-tertiary-500': '192 192 192', // #c0c0c0
		'--color-tertiary-600': '173 173 173', // #adadad
		'--color-tertiary-700': '144 144 144', // #909090
		'--color-tertiary-800': '115 115 115', // #737373
		'--color-tertiary-900': '94 94 94', // #5e5e5e
		// success | #12984f
		'--color-success-50': '219 240 229', // #dbf0e5
		'--color-success-100': '208 234 220', // #d0eadc
		'--color-success-200': '196 229 211', // #c4e5d3
		'--color-success-300': '160 214 185', // #a0d6b9
		'--color-success-400': '89 183 132', // #59b784
		'--color-success-500': '18 152 79', // #12984f
		'--color-success-600': '16 137 71', // #108947
		'--color-success-700': '14 114 59', // #0e723b
		'--color-success-800': '11 91 47', // #0b5b2f
		'--color-success-900': '9 74 39', // #094a27
		// warning | #ffff0b
		'--color-warning-50': '255 255 218', // #ffffda
		'--color-warning-100': '255 255 206', // #ffffce
		'--color-warning-200': '255 255 194', // #ffffc2
		'--color-warning-300': '255 255 157', // #ffff9d
		'--color-warning-400': '255 255 84', // #ffff54
		'--color-warning-500': '255 255 11', // #ffff0b
		'--color-warning-600': '230 230 10', // #e6e60a
		'--color-warning-700': '191 191 8', // #bfbf08
		'--color-warning-800': '153 153 7', // #999907
		'--color-warning-900': '125 125 5', // #7d7d05
		// error | #EA3000
		'--color-error-50': '252 224 217', // #fce0d9
		'--color-error-100': '251 214 204', // #fbd6cc
		'--color-error-200': '250 203 191', // #facbbf
		'--color-error-300': '247 172 153', // #f7ac99
		'--color-error-400': '240 110 77', // #f06e4d
		'--color-error-500': '234 48 0', // #EA3000
		'--color-error-600': '211 43 0', // #d32b00
		'--color-error-700': '176 36 0', // #b02400
		'--color-error-800': '140 29 0', // #8c1d00
		'--color-error-900': '115 24 0', // #731800
		// surface | #262626
		'--color-surface-50': '222 222 222', // #dedede
		'--color-surface-100': '212 212 212', // #d4d4d4
		'--color-surface-200': '201 201 201', // #c9c9c9
		'--color-surface-300': '168 168 168', // #a8a8a8
		'--color-surface-400': '103 103 103', // #676767
		'--color-surface-500': '38 38 38', // #262626
		'--color-surface-600': '34 34 34', // #222222
		'--color-surface-700': '29 29 29', // #1d1d1d
		'--color-surface-800': '23 23 23', // #171717
		'--color-surface-900': '19 19 19' // #131313
	}
};
