// place files you want to import through the `$lib` alias in this folder.

export const gitDomain = 'git.froth.zone';
export const gitName = 'Froth Git';
export const pagesDomain = 'pages.git.froth.zone';
export const pagesName = 'Froth Pages';
export const ipv4 = '150.136.112.69';
export const ipv6 = '2603:c020:4004:62ee:d84c:2488:5bdb:99c1';
